(function () {
  var createAppointmentTypeCtrl = function ($scope, crud_service, $state) {
      $scope.save = function(){

          $scope.appType.company_id =window.localStorage.getItem('companyId');
          var data = {'appointment_type': $scope.appType};
          var promise = crud_service.post_me('appointment_types',data);

          promise.then(function(response){
            $state.go('owner.appointmentTypes');
          },function(error){
              console.log("Reponse error", error);
          });
      };
  };
    App.controller('createAppointmentTypeCtrl',createAppointmentTypeCtrl)
}());