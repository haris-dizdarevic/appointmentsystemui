(function(){

    var newEventModalCtrl = function($scope, start, $modalInstance, crud_service, appointment_types) {

        $scope.types = appointment_types.data;
        $scope.selectedType = $scope.types[0];

    $scope.myAdd =  function(){
        console.log("Dodaje se app type id", $scope.selectedType.id);
        var ldata = {"appointment": {
            appointment_type_id: $scope.selectedType.id,
            start: start,
            end: "",
            client: $scope.user.name,
            email: $scope.user.email,
            phone: $scope.user.phone
        }};
        var promise = crud_service.post_me('appointments',ldata);
        promise.then(function(response){
            console.log("Dodavanje eventa modal response post",response);
            var newEvent = {
                id: response.data.id,
                title: $scope.user.name,
                start: moment(response.data.start).format(),
                end: moment(response.data.end).format(),
                allDay: false,
                overlap: false
            };
                $modalInstance.close(newEvent);
                $modalInstance.dismiss();

        }, function(error){
            console.log("response error backend", error);
        });


    };


    };

    App.controller('newEventModalCtrl',newEventModalCtrl);
}());