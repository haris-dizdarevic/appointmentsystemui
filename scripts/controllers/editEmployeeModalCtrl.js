(function () {
  var editEmployeeModalCtrl = function ($scope, crud_service, employee, $modalInstance, Flash) {
        $scope.user = employee;
        $scope.update = function () {
            $scope.user.role = 0;
            var data = {'user': $scope.user};
            var promise = crud_service.put_me('users/'+$scope.user.id,data);
            promise.then(function (response) {
                $modalInstance.close();
                Flash.create('success',"Employee successfully edited");
            }, function (error) {
                console.log("Error editing user");
            });
        }
  };
    App.controller('editEmployeeModalCtrl',editEmployeeModalCtrl)
}());