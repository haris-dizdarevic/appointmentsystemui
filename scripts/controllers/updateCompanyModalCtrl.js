(function () {
  var updateCompanyModalCtrl = function ($scope, crud_service, company, $modalInstance) {
        $scope.company = company;
        $scope.update = function () {
            var data = {'company':$scope.company};
            var promise = crud_service.put_me('companies/'+$scope.company.id, data);
            promise.then(function (response) {
                $modalInstance.close(response);
            });
        }
  };
    App.controller('updateCompanyModalCtrl', updateCompanyModalCtrl)
}());