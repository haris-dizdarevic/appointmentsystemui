(function () {
  var updateUserModalCtrl = function ($scope, user, crud_service, $modalInstance, allCompanies) {
      console.log('user',user);
      $scope.user = user;
      if($scope.user.role != 'rootAdmin'){
          $scope.showCompanies = true;
          $scope.selectedCompany = user.company;
      }
      $scope.companies = allCompanies;

      console.log(user);
      $scope.userTypes = [{id:0, name:'Employee'},{id:1, name:'Owner'},{id:2, name:'Admin'}];
      if(user.role == 'employee'){
          $scope.selectedRole = $scope.userTypes[0];
      }else if(user.role == 'owner'){
          $scope.selectedRole = $scope.userTypes[1];
      }else if(user.role == 'rootAdmin'){
          $scope.selectedRole = $scope.userTypes[2];
      }

      $scope.change = function () {
          if($scope.selectedRole == $scope.userTypes[2]){
              $scope.showCompanies = false;
          }else{
              $scope.showCompanies = true;
          }
      };


      console.log('selected',$scope.selectedRole);
      $scope.saveUser = function () {
          $scope.user.role = $scope.selectedRole.id;
          if($scope.user.role == 2){
              $scope.user.company_id = null;
          }else{
              $scope.user.company.id = $scope.selectedCompany.id;
          }
          var data = {'user': $scope.user};
          console.log('data',data);
          var promise = crud_service.put_me('users/'+ user.id, data);
          promise.then(function (response) {
              $modalInstance.close(response)
          },
              function (error) {
                  alert('Something went wrong');
              })
      }
  };
    App.controller('updateUserModalCtrl', updateUserModalCtrl)
}());