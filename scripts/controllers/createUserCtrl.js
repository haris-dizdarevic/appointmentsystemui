(function () {
  var createUserCtrl = function ($scope, crud_service, companies, $state, Flash) {
      $scope.userTypes = [{id:0, name:'Employee'},{id:1, name:'Owner'},{id:2, name:'Admin'}];
      $scope.selectedRole = $scope.userTypes[2];
      $scope.companies = companies.data;
      $scope.selectedCompany = $scope.companies[0];

      $scope.change = function () {
          if($scope.selectedRole == $scope.userTypes[2]){
              $scope.showCompanies = false;
          }else{
              $scope.showCompanies = true;
          }
      };

    $scope.createUser = function () {
        $scope.user.role = $scope.selectedRole.id;
        if ($scope.user.role == 2) {
            $scope.user.company_id = null;
        } else {
            $scope.user.company_id = $scope.selectedCompany.id;
        }
        console.log("User company_id: ",$scope.user.company_id);
        var data = {'user':$scope.user};
        var promise = crud_service.post_me('users', data);
        promise.then(function (response) {
            $state.go('admin.users');
            Flash.create('success','User successfully created');
        },
            function (error) {
                
            });
    }
  };

App.controller('createUserCtrl', createUserCtrl)
}());