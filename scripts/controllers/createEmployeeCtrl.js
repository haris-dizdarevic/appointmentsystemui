(function () {
  var createEmployeeCtrl = function ($scope, crud_service, $state, Flash) {
      $scope.saveEmployee = function () {
          $scope.user.role = 0;
          var data = {'user':$scope.user};
          var promise = crud_service.post_me('users',data);
          promise.then(function (response) {
           $state.go('owner.employees');
           Flash.create('success','Employee successfully added');
          })
      }
  };
    App.controller('createEmployeeCtrl', createEmployeeCtrl)
}());