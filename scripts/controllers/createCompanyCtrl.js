(function () {
   var createCompanyCtrl = function ($scope, crud_service,$state, Flash) {
       $scope.createCompany = function () {
           var data = {'company': $scope.company};
           var promise = crud_service.post_me('companies', data);
           promise.then(function (response) {
               Flash.create('success','Company successfully created');
               $state.go('admin.companies');
           },
               function (error) {
                   alert('Something went wrong');
               });
       };
   };
    App.controller('createCompanyCtrl',createCompanyCtrl)
}());