(function(){

    var editProfileCtrl = function($scope,user,crud_service, $state){
        $scope.user = user.data;


        $scope.editProfile = function () {
            var data = {"user": $scope.user};
            crud_service.put_me('users/'+$scope.user.id,data).then(function (response) {
                console.log('response',response);
                if(response.data.role == 'owner'){
                    $state.go('owner');
                }
                if(response.data.role == 'rootAdmin'){
                    $state.go('admin');
                }
                if(response.data.role == 'employee'){
                    $state.go('employee');
                }
            },function(error){})

        }
    };

    App.controller('editProfileCtrl',editProfileCtrl);
}());