(function(){

    var userModalCtrl = function($scope, $modalInstance, crud_service, $stateParams, employee){
        var edit = false;
        if (employee != null) {
            $scope.e = employee;
            if(employee.role == "employee") {
                $scope.selectedRole = "0";
            } else if (employee.role == "owner") {
                $scope.selectedRole = "1";
            }
            edit = true;
        }

        $scope.save = function(){
            console.log("Edit",edit);
            $scope.e.company_id = $stateParams.id;
            $scope.e.role = parseInt($scope.selectedRole);
            var data = {"user": $scope.e};
            if (edit) {
                var promise = crud_service.put_me('users/'+$scope.e.id,data);
            } else {
                var promise = crud_service.post_me('users',data);
            }

            promise.then(function(response){
                console.log("reponmse",response);
                $modalInstance.close(response.data);
                $modalInstance.dismiss();
            },function(error){
                console.log("Error from modal");
            });
        };

        //$scope.update = function(){
        //    $scope.e.company_id = $stateParams.id;
        //    $scope.e.role = parseInt($scope.selectedRole);
        //    var data = {"user": $scope.e};
        //    var promise = crud_service.put_me('users',data);
        //    promise.then(function(response){
        //        console.log("reponmse",response);
        //        $modalInstance.close(response.data);
        //        $modalInstance.dismiss();
        //    },function(error){
        //        console.log("Error from modal");
        //    });
        //};


    };

    App.controller('userModalCtrl',userModalCtrl);
}());