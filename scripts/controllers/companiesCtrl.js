(function(){

    var companiesCtrl = function($scope, companies, crud_service, $modal,Flash){
        console.log("Companies ctrl",companies);
        $scope.companies = companies.data;
        $scope.sortType = 'company_name';
        $scope.sortReverse = false;
        $scope.companyUpdate = function (company) {

            var data = {'company': company};
            console.log("data",data);
            console.log('company',company);
            var promise = crud_service.put_me('companies/'+company.id, data);
            promise.then(function (response) {
                Flash.create('success','Company successfully edited');
                getCompanies();
            });
        };

        $scope.deleteCompany = function (company) {
            if(confirm('Are You sure you want delete '+company.name+' ?'))
          var promise = crud_service.delete_me('companies/'+company.id);
            promise.then(function (response) {
                Flash.create('success','Company successfully deleted');
                getCompanies();
            })
        };

        $scope.editCompany = function (company) {
          var modalInstance = $modal.open({
              templateUrl: 'views/updateCompanyModal.html',
              resolve: {
                  company: function(){
                      return angular.copy(company);
                  }
              },
              controller: 'updateCompanyModalCtrl'
          });
            modalInstance.result.then(function (response) {
                Flash.create('success','Company successfully edited');
                getCompanies();
            })
        };

        function getCompanies(){
            var promise = crud_service.get_me('companies');
            promise.then(function (response) {
                $scope.companies = response.data;
            },
                function (error) {
                    alert('Something went wrong');
                });
        }

    };

    App.controller('companiesCtrl',companiesCtrl)
}());