(function(){


    var userCtrl = function($scope,  employees, $modal, crud_service, $stateParams){
        if (employees != null) {
            $scope.employees = employees.data;
        }

        $scope.addNew = function(){
            var modalInstance = $modal.open({
                templateUrl: 'views/formUser.html',
                size: 'modal-lg',
                resolve: {
                    employee: function(){
                        return null;
                    }
                },
                controller: 'userModalCtrl'
            });

            modalInstance.result.then(function(response){
                $scope.employees.push(response);
            },function(){});
        };


        $scope.removeUser = function(id){
            var promise = crud_service.delete_me('users/'+id);
            promise.then(function(response){
                console.log("removed");
                //remove from $scope.employees
            },function(error){});
        };

        $scope.editUser = function(employee){
            var modalInstance = $modal.open({
                templateUrl: 'views/formUser.html',
                size: 'modal-lg',
                resolve: {
                    employee: function(){
                        var resolvedEmployee = angular.copy(employee);
                        return resolvedEmployee;
                    }
                },
                controller: 'userModalCtrl'
            });

            modalInstance.result.then(function(response){
                getEmployees();
            },function(){});
        };

        function getEmployees(){
            var promise = crud_service.get_me('users?company_id=' + $stateParams.id);
            promise.then(function (response) {
                $scope.employees = response.data;
            },
                function (error) {

                });
        }
    };


    App.controller('userCtrl',userCtrl);
}());