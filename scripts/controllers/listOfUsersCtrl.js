(function () {
   var listOfUsersCtrl = function ($scope, users, crud_service, $modal, companies, Flash) {
       $scope.companies = companies.data;
       $scope.users = users.data;
       $scope.sortType = 'first_name';
       $scope.sortReverse = false;

       $scope.editUser = function (user) {
           var modalInstance = $modal.open({
               templateUrl: 'views/updateUserModal.html',
               resolve:{
                   user: function () {
                       var userCopy = angular.copy(user);
                       return userCopy
                   },
                   allCompanies: function () {
                       return $scope.companies;
                   }

               },
               controller: 'updateUserModalCtrl'
           });

           modalInstance.result.then(function () {
               Flash.create('success','User successfully edited');
               getUsers();

           });
       };

       $scope.deleteUser = function (user) {
           if(confirm('Are you sure u want delete ' +user.first_name +' '+user.last_name + ' ?')){
               var promise = crud_service.delete_me('users/'+user.id);
               promise.then(function (response) {
                       Flash.create('success','User successfully deleted');
                       getUsers();
                   },
                   function (error) {
                       alert('Something went wrong!!!');
                   });
           }

       };

       function getUsers(){
           var promise = crud_service.get_me('users');
           promise.then(function (response) {
               $scope.users = response.data;
           })
       }
   };
    App.controller('listOfUsersCtrl',listOfUsersCtrl)
}());