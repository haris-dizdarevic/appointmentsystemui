(function(){

    var appointmentTypeCtrl = function($scope, crud_service, appointment_types, $stateParams, $modal){
        $scope.appointmentTypes = appointment_types.data;
        $scope.sortType = 'name';
        $scope.sortReverse = false;
        $scope.addNew = function(){
            var modalInstance = $modal.open({
                templateUrl: 'views/appointmentTypeModal.html',
                size: 'modal-lg',
                resolve: { appType: function () {return null;} },
                controller: 'appointmentModalCtrl'
            });

            modalInstance.result.then(function(response){
                console.log("Modal send response:",response);
                $scope.appointmentTypes.push(response);
            },function(error){
                console.log("Error");
            });
        };

        $scope.editType = function(appType){
            var modalInstance = $modal.open({
                templateUrl: 'views/appointmentTypeEditModal.html',
                size: 'modal-lg',
                resolve: {
                    appType: function(){
                        var type = angular.copy(appType);
                        return type;
                    }
                },
                controller: 'appointmentModalCtrl'
            });

            modalInstance.result.then(function(response){
                getAppTypes();
            },function(error){

            });
        };

        $scope.deleteType = function(appointmentType){
            if(confirm('Are You sure you want to delete'+appointmentType.name+'?')){
                var promise = crud_service.delete_me('appointment_types/'+appointmentType.id);
                promise.then(function(response){
                    getAppTypes();
                }, function(error){
                    console.log("Error deleteType");
                });
            }

        };

        function getAppTypes(){
            var promise = crud_service.get_me('appointment_types');
            promise.then(function (response) {
                    $scope.appointmentTypes = response.data;
            },
                function (error) {
                    alert('Error on getAppTypes');
                });
        }


    };
    App.controller('appointmentTypeCtrl',appointmentTypeCtrl);
}());