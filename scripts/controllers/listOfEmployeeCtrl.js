(function () {
  var listOfEmployeeCtrl = function ($scope, crud_service, employees, $modal) {
      $scope.employees = employees.data;
      $scope.sortType = 'first_name';
      $scope.sortReverse = false;

      $scope.editEmployee = function (employee) {
         var modalInstance = $modal.open({
              templateUrl: 'views/editEmployeeModal.html',
              resolve: {
                  employee: function () {
                      var employeeCopy = angular.copy(employee);
                      return employeeCopy;
                  }
              },
              controller: 'editEmployeeModalCtrl'
          });
          modalInstance.result.then(function (result) {
              getEmployees();
          });
      };

      $scope.deleteEmployee = function (employee) {
          if(confirm('Are You sure you want delete '+employee.first_name+' '+employee.last_name+' ?')){
              var promise = crud_service.delete_me('users/'+employee.id);
              promise.then(function (response) {
                  getEmployees();
              })
          }
      };

      function getEmployees(){
          var promise = crud_service.get_me('users');
          promise.then(function (response) {
              $scope.employees = response.data;
          });
      }
  };
    App.controller('listOfEmployeeCtrl', listOfEmployeeCtrl)
}());