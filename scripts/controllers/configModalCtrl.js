(function(){


    var configModalCtrl = function($scope, company, crud_service, $modalInstance){
        $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.mytime1 = new Date();
        $scope.hstep1 = 1;
        $scope.mstep1 = 1;

        $scope.days = [{id: 0, name: 'Mon', checked: false},{id: 1, name: 'Tue', checked: false},{id: 2, name: 'Wed', checked: false},
            {id: 3, name: 'Thu', checked: false}, {id: 4, name: 'Fri', checked: false},{id: 5, name: 'Sat', checked:false},
            {id: 6, name: 'Sun', checked: false}];

        $scope.isChecked = function(day){
            if (day.checked) {
                return 'active';
            } else return '';
        };

        $scope.confirm = function(){
            company.minTime = ""+$scope.mytime.getHours()+":"+$scope.mytime.getMinutes();
            console.log("vrijeme",company.minTime);
            company.hiddenDays = "";
            company.maxTime = ""+$scope.mytime1.getHours()+":"+$scope.mytime1.getMinutes();
            for(var i=0; i<$scope.days.length; i++) {
                if ($scope.days[i].checked == true) {
                    company.hiddenDays += ""+$scope.days[i].id+"";
                }
            }
            var promise = crud_service.put_me('companies/'+company.id, company);
            promise.then(function(response){
                $modalInstance.close();
                $modalInstance.dismiss();
            },function(error){});


        };

    };

    App.controller('configModalCtrl',configModalCtrl);
}());