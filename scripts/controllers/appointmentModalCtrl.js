(function(){

    var appointmentModalCtrl = function($scope, $modalInstance, crud_service, $stateParams, appType){

        if (appType != null) {
            $scope.appType = appType;
        }


        $scope.update = function(){
            var data = {'appointment_type': $scope.appType};
            var promise = crud_service.put_me('appointment_types/'+$scope.appType.id,data);

            promise.then(function(response){
                console.log("Response update success",response);
                $modalInstance.close(response.data);
            },function(error){
                console.log("Reponse error", error);
            });
        };


    };


    App.controller('appointmentModalCtrl',appointmentModalCtrl);
}());