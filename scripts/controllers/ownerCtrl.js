(function () {
    var ownerCtrl = function ($scope, company) {
        $scope.isCollapsed = false;
        $scope.isCollapsedEmployee = false;

        $scope.company = company.data;

    };
    App.controller('ownerCtrl', ownerCtrl)
}());