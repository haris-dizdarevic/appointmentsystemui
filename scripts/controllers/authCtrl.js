(function(){

    var authCtrl = function($scope, crud_service, $location, $modal){

        $scope.logUser = function(){
            var data = {"access":$scope.user};
            var promise = crud_service.post_me('access',data);
            promise.then(function(response){
                console.log("response login", response);
                window.localStorage.setItem('token',response.data.auth_token);
                window.localStorage.setItem('userId',response.data.user.id);

                if (response.data.user.role == 'employee') {
                    window.localStorage.setItem('companyId',response.data.user.company.id);
                    $location.path('/home/appointments');
                } else if (response.data.user.role == 'owner') {
                    window.localStorage.setItem('companyId',response.data.user.company.id);
                    if(response.data.user.company.minTime == null) {
                        var modalInstance = $modal.open({
                            templateUrl: 'views/config.html',
                            size: 'modal-lg',
                            controller: 'configModalCtrl',
                            resolve: {
                                company: function(){
                                    return response.data.user.company;
                                }
                            }
                        });
                        modalInstance.result.then(function(modal_response){
                            $location.path('/owner');
                        },function(error){
                            window.localStorage.clear();
                            $location.path('/login');
                        });
                    } else $location.path('/owner');

                }  else if (response.data.user.role == 'rootAdmin'){
                    $location.path('/admin');
                }
            },function(error){});

};

        $scope.logout = function(){
            var promise = crud_service.delete_me('access/'+window.localStorage.userId);
            promise.then(function(response){
                window.localStorage.clear();
                $location.path('/login');
            },function(error){});
        };

    };

    App.controller('authCtrl',authCtrl);
}());
