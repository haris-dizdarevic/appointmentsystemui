var App = angular.module('App', ['ui.router','ui.calendar','ui.bootstrap','flash', 'angularUtils.directives.dirPagination']);

App.config(function($stateProvider, $urlRouterProvider, $httpProvider){

    $urlRouterProvider.otherwise('/login');

    $stateProvider

    .state('login',{
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'authCtrl'
    })
    .state('admin',{
            url: '/admin',
            templateUrl: 'views/rootAdmin.html',
            controller: 'rootAdminCtrl'
        })
    .state('admin.newCompany', {
            url: '/create/company',
            templateUrl: 'views/createCompany.html',
            controller: 'createCompanyCtrl'
        })
    .state('admin.newUser',{
        url: '/create/user',
        templateUrl: 'views/createUser.html',
        resolve:{
            companies: function (crud_service) {
                return crud_service.get_me('companies');
            }
        },
        controller: 'createUserCtrl'
    })
    .state('admin.users',{
        url: '/users',
        templateUrl: 'views/listOfUsers.html',
        resolve: {
            users: function(crud_service){
                return crud_service.get_me('users');
            },
            companies: function (crud_service) {
                return crud_service.get_me('companies');
            }
        },
        controller: 'listOfUsersCtrl'
    })
    .state('admin.companies',{
        url: '/companies',
        templateUrl: 'views/companies.html',
        resolve: {
            companies: function(crud_service){
                return crud_service.get_me('companies');
            }
        },
        controller: 'companiesCtrl'
    })
        .state('admin.edit_profile',{
            url: '/edit_profile',
            templateUrl: 'views/editProfile.html',
            resolve:{
                user: function (crud_service) {
                    return crud_service.get_me('users/'+window.localStorage.getItem('userId'));
                }
            },
            controller: 'editProfileCtrl'
        })
    .state('owner',{
        url: '/owner',
        templateUrl: 'views/owner.html',
        resolve: {
            company: function(crud_service){
                return crud_service.get_me('companies/'+window.localStorage.getItem('companyId'));
            }
        },
        controller: 'ownerCtrl'
    })
    .state('owner.appointmentTypes',{
        url: '/appointment_types',
        templateUrl: 'views/appointmentType.html',
        resolve:{
            appointment_types: function (crud_service) {
                return crud_service.get_me('appointment_types');
            }
        },
        controller: 'appointmentTypeCtrl'
    })
    .state('owner.newAppointmentType',{
        url: '/create/appointment_type',
        templateUrl: 'views/createAppointmentType.html',
        controller: 'createAppointmentTypeCtrl'
    })
    .state('owner.employees',{
        url: '/employees',
        templateUrl: 'views/listOfEmployee.html',
        resolve: {
            employees: function (crud_service) {
                return crud_service.get_me('users');
            }
        },
        controller: 'listOfEmployeeCtrl'
    })
    .state('owner.newEmployee',{
        url: '/create/employee',
        templateUrl: 'views/createEmployee.html',
        controller: 'createEmployeeCtrl'
    })
    .state('owner.edit_profile',{
        url: '/edit_profile',
        templateUrl: 'views/editProfile.html',
        resolve:{
            user: function (crud_service) {
                return crud_service.get_me('users/'+window.localStorage.getItem('userId'));
            }
        },
        controller: 'editProfileCtrl'
    })
    .state('employee',{
        url: '/home',
        templateUrl: 'views/employee.html',
        resolve: {
            company: function(crud_service){
                return crud_service.get_me('companies/'+window.localStorage.getItem('companyId'));
            }
        },
        controller: function ($scope, company) {
            $scope.company = company.data;
        }
    })
        .state('employee.appointments',{
            url:'/appointments',
            templateUrl: 'views/calendar.html',
            controller: 'CalendarCtrl'
        }).
    state('employee.about',{
            url: '/about',
            templateUrl: 'views/about.html',
            controller : function(){}
        })
    .state('employee.edit_profile',{
        url: '/edit_profile',
        templateUrl: 'views/editProfile.html',
        resolve:{
            user: function (crud_service) {
                return crud_service.get_me('users/'+window.localStorage.getItem('userId'));
            }
        },
        controller: 'editProfileCtrl'
    })
    .state('logout',{
        url: '/logout',
        controller: function (crud_service, $state) {
            crud_service.delete_me('access/'+window.localStorage.getItem('userId')).then(function () {
                window.localStorage.clear();
                $state.go('login')
            },function(){});
        }
    });

    $httpProvider.interceptors.push('intercepter');

});

App.run(["$rootScope", function($rootScope){
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
            $rootScope.logged = false;
            if (window.localStorage.getItem('token')!= undefined) {
                $rootScope.logged = true;
            }
        });

}]);

